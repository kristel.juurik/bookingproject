package BookingTests.utilFiles;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class SearchHotel {
    public static void findSimilarHotels(WebDriver driver, WebDriverWait wait, JavascriptExecutor js, String HOTEL) {

        driver.findElement(By.cssSelector("#b2indexPage")).click();
        driver.findElement(By.cssSelector("#ss")).click();
        driver.findElement(By.cssSelector("#ss")).sendKeys(HOTEL);
        //Check-in date
        WebElement element3 = driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(2) > div > div > div > div > span"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element3);
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(2) > div > div > div > div > span")).click();
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(6) > span > span")).click();
        //check-out date
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(7) > span > span")).click();
        //Search button
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__button > div.sb-searchbox-submit-col.-submit-button > button")).click();
        //choose hotel
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement element5 = driver.findElement(By.cssSelector("#b2searchresultsPage"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element5);

        driver.findElement(By.className("bui-button__text")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //use the new opened window
        String parentWindow = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String windowHandle : handles) {
            if (!windowHandle.equals(parentWindow)) {
                driver.switchTo().window(windowHandle);

                WebElement element = driver.findElement(By.cssSelector("#left > div.hp_sidebar_usp_box.hp-sidebar-block.hp_sidebar-similar > a"));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

                driver.findElement(By.cssSelector("#left > div.hp_sidebar_usp_box.hp-sidebar-block.hp_sidebar-similar > a")).click();

                //driver.close(); //closing child window
                //driver.switchTo().window(parentWindow); //control to parent window
            }

        }

    }
}
