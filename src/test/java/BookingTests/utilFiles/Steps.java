package BookingTests.utilFiles;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static BookingTests.utilFiles.TestCommons.*;

public class Steps {
    public static void search(WebDriver driver, WebDriverWait wait, JavascriptExecutor js, String HOTEL) {
        driver.get(BOOKING_URL);
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector("#ss")).click();
        driver.findElement(By.cssSelector("#ss")).sendKeys(HOTEL);
        //Check-in date
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(2) > div > div > div > div > span")).click();
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(6) > span > span")).click();
        //check-out date
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(7) > span > span")).click();
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__button > div.sb-searchbox-submit-col.-submit-button > button")).click();


    }
}