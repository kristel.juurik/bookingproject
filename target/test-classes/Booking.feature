Feature: Booking
  As a user, I want to be able to login to Booking.com with a valid email and valid password to access my account.

  Scenario:
    Given I am on Home Page
    When I click on Sign In Button
    Then I am on Login Screen
    When I enter "kristel.juurik@gmail.com" in the email address field
    And I click Next Button
    Then I am on Login Screen
    When I enter this "kjr543uy" in the password field
    And I click Sign In Button
    Then I am on Home Page as Registered User


