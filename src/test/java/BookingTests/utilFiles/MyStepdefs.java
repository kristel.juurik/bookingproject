package BookingTests.utilFiles;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static BookingTests.utilFiles.TestCommons.*;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;

public class MyStepdefs extends TestTemplate {

    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, 20);


    @Before
    public void setUp() {
        set();
        driver.manage().window().maximize();
        driver.navigate().to(BOOKING_URL);
        driver.get(BOOKING_URL);
    }

    @AfterAll
    public void tearDown() {
        driver.quit();
    }



    @Given("I am on Home Page")
    public void iAmOnHomePage() {
        driver.manage().window().maximize();
        driver.navigate().to(BOOKING_URL);
    }

    @When("I click on Sign In Button")
    public void iClickOnSignInButton() {
        driver.findElement(By.cssSelector("#current_account > a > div > span")).click();
    }

    @Then("I am on Login Screen")
    public void iAmOnLoginScreen() {
        assertEquals(driver.getTitle(), LOGIN_TITLE);
        driver.manage().timeouts().pageLoadTimeout(20, SECONDS);
    }

    @When("I enter {string} in the email address field")
    public void iEnterInTheEmailField(String EMAIL) throws InterruptedException {
        EMAIL = "kristel.juurik@gmail.com";
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys(EMAIL);
        assertEquals(driver.getTitle(), LOGIN_TITLE);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @And("I click Next Button")
    public void iClickNextButton() {
        driver.findElement(By.cssSelector("#root > div > div.app > div.access-container.bui_font_body > div > div > div > div > div > div > form > div:nth-child(3) > button > span")).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        assertEquals(driver.getTitle(), LOGIN_TITLE);
    }

    @When("I enter this {string} in the password field")
    public void iEnterInThePasswordField(String PASSWORD) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        PASSWORD = "kjr543uy";
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys(PASSWORD);
        assertEquals(driver.getTitle(), LOGIN_TITLE);
    }
    @And("I click Sign In Button")
    public void iClickSignInButton () {
        driver.findElement(By.cssSelector("#root > div > div.app > div.access-container.bui_font_body > div > div > div > div > div > div > form > button > span")).click();
    }

    @Then("I am on Home Page as Registered User")
    public void iAmOnHomePageAsRegisteredUser () {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#b2indexPage")));
        assertEquals(driver.getTitle(), HOME_TITLE);
    }

}
