package BookingTests.utilFiles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static BookingTests.utilFiles.TestCommons.BOOKING_URL;

public class HotelLocation {
    public static void findHotelLocation(WebDriver driver, String HOTEL) {

        driver.get(BOOKING_URL);
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector("#b2indexPage")).click();
        driver.findElement(By.cssSelector("#ss")).click();
        driver.findElement(By.cssSelector("#ss")).sendKeys(HOTEL);
        //Check-in date
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(2) > div > div > div > div > span")).click();
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(6) > span > span")).click();
        //check-out date
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table > tbody > tr:nth-child(4) > td:nth-child(7) > span > span")).click();
        //Search button
        driver.findElement(By.cssSelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__button > div.sb-searchbox-submit-col.-submit-button > button")).click();
        //choose hotel
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.className("bui-button__text")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //use the new opened window
        String parentWindow = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String windowHandle : handles) {
            if (!windowHandle.equals(parentWindow)) {
                driver.switchTo().window(windowHandle);

                driver.findElement(By.id("hotel_header")).click();

                String myText;
                myText = driver.findElement(By.cssSelector("#b_map_container > div.hp_map_elements_container > div.iw-control.js-iw-control.maps_iw_side > div > div.maps_iw__container_fixed > div.maps_iw__address.maps_iw_text_style__light")).getText();
                System.out.println("Hestia Hotel address :  " + myText);



            }
        }
    }
}