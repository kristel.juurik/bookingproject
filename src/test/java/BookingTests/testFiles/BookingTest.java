package BookingTests.testFiles;

import BookingTests.utilFiles.LoginSuccessful;
import BookingTests.utilFiles.TestTemplate;
import org.junit.jupiter.api.*;

import static BookingTests.utilFiles.TestCommons.LOGIN_TITLE;
import static org.junit.Assert.assertEquals;

public class BookingTest extends TestTemplate {
    @BeforeAll
    public void setUp() {
        set();
        driver.manage().window().maximize();
    }


    @AfterAll
    public void tearDown() {
        driver.quit();
    }


    /*@BeforeEach
    public void startAgain() {
        set();
        driver.manage().window().maximize();
    }

     */

    @AfterEach
    public void closeWindow() {
        log.info("Close window");
        driver.close();
    }


    /*@Test
    public void findSimilarHotels() {
        SearchHotel.findSimilarHotels(driver, wait, js, HOTEL);
    }

     */

    @Test
    @DisplayName("Login is successful with correct User and Password")
    public void login() {
        LoginSuccessful.loginFunction(driver);
        assertEquals(driver.getTitle(), LOGIN_TITLE);
    }
}
