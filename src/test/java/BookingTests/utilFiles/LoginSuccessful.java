package BookingTests.utilFiles;

import org.openqa.selenium.*;

import java.util.concurrent.TimeUnit;

public class LoginSuccessful {
    public static void loginFunction(WebDriver driver) {
        driver.get("https://account.booking.com/sign-in?op_token=EgVvYXV0aCKUBQoUdk8xS2Jsazd4WDl0VW4yY3BaTFMSCWF1dGhvcml6ZRo1aHR0cHM6Ly9zZWN1cmUuYm9va2luZy5jb20vbG9naW4uaHRtbD9vcD1vYXV0aF9yZXR1cm4qswRVcU1ENFh0dGlsLWMyWUpaV1JzNUc2NEl6ODlmSG4xX0VhVDUteXZFdEttaTBUMG5fRGVmOFBoYnNiczltSmxLdm5sUE5EWk0zUGFxeFFROFlia0E4N0V3d3lZUVNkY29JSnVpVkg4OGlETkhzZEszelloMHpBTG5wLUV4bFV1OG9aRE5wNDNCQ0xhVGJXb0ZlQnE5RmRPVnM1WnFmZHRnaDBCWGl3T0E5RU1JLXNmUkRYVGVicU91ZWt5THRLSVg5RTdqN3hKam5Gbmd5Zm1Ddmw3eS05V1k3ZTZZaGljTUd1OEIyclpZajJjQ1hnakpoZTVYNXlJaG1BT3lybG1oNHoyUlloMWZFci1Ea2JpaXdxalRHQ0FITkFyTE9SaTBfcDg4cWNaekRKbTRZclllYUw2c0dMcUlxZzktSnhsMFpSMjBmWUdfNm14cTVvaVZuRS1neWVrMmVFaTM5c2lXaHFuWXRJcVZaRGl6Q2VLTXFtaVdKTkFfQjZ1OXJOMXNkVndXNjVWcElLSlhoWnktYWF3OEQwMmlrRS1HTEFmY3luZU9aRnB1cTRLQWpTa1Q4bU9jODcwSFZKekNoYThBUmRnUEt6dHpiZF9YZUxLaGl6MzFweml4S2JmYnExMFdZV1g2MUJSaWRuTE5WbS1LdDVrWk11cmwwNjlfZ2x0TzUzWlhRMGhKWXFWZFp2dHJJZXVmQ3dHNU1sSDlvWnc3b2NJbXdRekttbkMwNjgybldTMEIEY29kZSoWCI7IEjDF7rPgmb4kOgBCAFibpJD9BQ");
        driver.manage().window().maximize();
        driver.findElement(By.id("username")).sendKeys("kristel.juurik@gmail.com");
        driver.findElement(By.cssSelector("#root > div > div.app > div.access-container.bui_font_body > div > div > div > div > div > div > form > div:nth-child(3) > button > span")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement element = driver.findElement(By.id("password"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

        driver.findElement(By.id("password")).sendKeys("kjr543uy");
        driver.findElement(By.cssSelector(".bui-button:nth-child(3) > .bui-button__text")).click();
    }


}
