package BookingTests.utilFiles;

public class TestCommons {
    //URLs
    public static final String BOOKING_URL="https://www.booking.com/";

    //Page title
    public static final String HOME_TITLE = "Booking.com | Official site | The best hotels & accommodation";
    public static final String LOGIN_TITLE= "Booking.com Account";

    //Strings
    public static final String HOTEL = "Hestia Hotel Ilmarine";
    public static final String CHECK_IN = "21.11";
    public static final String CHECK_OUT = "22.11";
    public static final String CLIENT_FIRST = "Nobody";
    public static final String CLIENT_LAST = "Noone";
    public static final String EMAIL = "kristel.juurik@gmail.com";
    public static final String ADDRESS = "Actually, I changed my mind.";




}
